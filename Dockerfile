#VERSÃO DO NODE
FROM node:16 AS builder
#CRIANDO DIRETORIO DA API
WORKDIR /app
#COPIANDO PARTES DO PROJETO PARA PASTA
COPY package*.json ./
COPY prisma ./prisma/
#INSTALAR DEPENDENCIAS
RUN npm install
RUN npx prisma generate
#COPIANDO DE UM PRA OUTRO
COPY . .
#RODANDO O BUILD DO PROJETO
RUN npm run build
#SELECIONANDO VERSÃO DO NODE
FROM node:16
#COPIA PARA EXECUSÃO
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package*.json ./
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/secrets ./secrets
#PORTA DO SERVIDOR
EXPOSE 3000
#COMANDO DE INICIO
CMD [ "npm", "run", "start:prod" ]

#docker build -t nest-caramelo-run . //CRIAR IMAGEM 
#docker run -p3000:3000 nest-caramelo-run // CRIA IMAGEM DA API É SOBE AO DOCKER