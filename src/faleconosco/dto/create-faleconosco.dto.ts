import { ApiProperty } from '@nestjs/swagger';

export class CreateFaleconoscoDto {
  @ApiProperty()
  nome: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  duvida: string;
  @ApiProperty()
  telefone: string;
}
