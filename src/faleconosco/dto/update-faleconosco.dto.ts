import { PartialType } from '@nestjs/swagger';
import { CreateFaleconoscoDto } from './create-faleconosco.dto';

export class UpdateFaleconoscoDto extends PartialType(CreateFaleconoscoDto) {}
