import { Injectable } from '@nestjs/common';
import { CreateFaleconoscoDto } from './dto/create-faleconosco.dto';
import { UpdateFaleconoscoDto } from './dto/update-faleconosco.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class FaleconoscoService {
  constructor(private prisma: PrismaService) {}
  create(createFaleconoscoDto: CreateFaleconoscoDto) {
    return this.prisma.faleconosco.create({ data: createFaleconoscoDto });
  }

  findAll() {
    return this.prisma.faleconosco.findMany();
  }

  findOne(id: number) {
    return this.prisma.faleconosco.findUnique({ where: { id } });
  }

  update(id: number, updateFaleconoscoDto: UpdateFaleconoscoDto) {
    return this.prisma.faleconosco.update({
      where: { id },
      data: updateFaleconoscoDto,
    });
  }

  remove(id: number) {
    return this.prisma.faleconosco.delete({ where: { id } });
  }
}
