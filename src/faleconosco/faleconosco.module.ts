import { Module } from '@nestjs/common';
import { FaleconoscoService } from './faleconosco.service';
import { FaleconoscoController } from './faleconosco.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [FaleconoscoController],
  providers: [FaleconoscoService],
  imports: [PrismaModule],
})
export class FaleconoscoModule {}
