import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { FaleconoscoService } from './faleconosco.service';
import { CreateFaleconoscoDto } from './dto/create-faleconosco.dto';
import { UpdateFaleconoscoDto } from './dto/update-faleconosco.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@Controller('faleconosco')
@ApiTags('FaleConosco')
export class FaleconoscoController {
  constructor(private readonly faleconoscoService: FaleconoscoService) {}

  @Post('criar')
  create(@Body() createFaleconoscoDto: CreateFaleconoscoDto) {
    return this.faleconoscoService.create(createFaleconoscoDto);
  }

  @Get('listar-todos')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll() {
    return this.faleconoscoService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.faleconoscoService.findOne(+id);
  }

  @Patch('atualizar/:id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(
    @Param('id') id: string,
    @Body() updateFaleconoscoDto: UpdateFaleconoscoDto,
  ) {
    return this.faleconoscoService.update(+id, updateFaleconoscoDto);
  }

  @Delete('deletar/:id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.faleconoscoService.remove(+id);
  }
}
