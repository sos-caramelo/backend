import { Test, TestingModule } from '@nestjs/testing';
import { FaleconoscoService } from './faleconosco.service';

describe('FaleconoscoService', () => {
  let service: FaleconoscoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FaleconoscoService],
    }).compile();

    service = module.get<FaleconoscoService>(FaleconoscoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
