import { ApiProperty } from '@nestjs/swagger';

export class Faleconosco {
  @ApiProperty()
  id: number;
  @ApiProperty()
  nome: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  duvida: string;
  @ApiProperty()
  telefone: string;
}
