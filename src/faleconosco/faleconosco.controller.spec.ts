import { Test, TestingModule } from '@nestjs/testing';
import { FaleconoscoController } from './faleconosco.controller';
import { FaleconoscoService } from './faleconosco.service';

describe('FaleconoscoController', () => {
  let controller: FaleconoscoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FaleconoscoController],
      providers: [FaleconoscoService],
    }).compile();

    controller = module.get<FaleconoscoController>(FaleconoscoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
