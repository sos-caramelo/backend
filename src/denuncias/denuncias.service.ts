import { Injectable } from '@nestjs/common';
import { CreateDenunciaDto } from './dto/create-denuncia.dto';
import { UpdateDenunciaDto } from './dto/update-denuncia.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class DenunciasService {
  constructor(private prisma: PrismaService) {}
  create(createDenunciaDto: CreateDenunciaDto) {
    return this.prisma.denuncia.create({ data: createDenunciaDto });
  }

  findAll() {
    return this.prisma.denuncia.findMany();
  }

  findOne(id: number) {
    return this.prisma.denuncia.findUnique({ where: { id } });
  }

  update(id: number, updateDenunciaDto: UpdateDenunciaDto) {
    return this.prisma.faleconosco.update({
      where: { id },
      data: updateDenunciaDto,
    });
  }

  remove(id: number) {
    return this.prisma.faleconosco.delete({ where: { id } });
  }
}
