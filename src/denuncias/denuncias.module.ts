import { Module } from '@nestjs/common';
import { DenunciasService } from './denuncias.service';
import { DenunciasController } from './denuncias.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [DenunciasController],
  providers: [DenunciasService],
  imports: [PrismaModule],
})
export class DenunciasModule {}
