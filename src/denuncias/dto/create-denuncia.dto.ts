import { ApiProperty } from '@nestjs/swagger';

export class CreateDenunciaDto {
  @ApiProperty()
  nome: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  endereco: string;
  @ApiProperty()
  descricao: string;
  @ApiProperty()
  Imagem: string;
}
