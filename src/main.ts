/* eslint-disable prettier/prettier */
// src/main.ts

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as fs from 'fs';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('SoS Caramelo')
    .setDescription('Projeto da Turma de 308 de TDS SENAI')
    .setVersion('0.2')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);

  const customOptions = {
    lang: 'pt', // Idioma Português
    customfavIcon: '<path>/favicon.png', //adding our favicon to swagger
    customSiteTitle: 'Documentação SoS Caramelo', //add site title to swagger for nice SEO
    customCss: `
    .topbar-wrapper img {content:url(\'https://cdn.icon-icons.com/icons2/2225/PNG/512/puppy_icon_134477.png\'); width:200px; height:auto;}
    .swagger-ui .topbar { background-color: #eead2d; } `,
    swaggerOptions: {
    persistAuthorization: true, // this helps to retain the token even after refreshing the (swagger UI web page)
    // swaggerOptions: { defaultModelsExpandDepth: -1 } //uncomment this line to stop seeing the schema on swagger ui
    }
    }


  SwaggerModule.setup(`api`, app, document, customOptions  );
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
