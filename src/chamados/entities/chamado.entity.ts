/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';

export class Chamado {
  @ApiProperty()
  id: number;
  @ApiProperty()
  nomeAnimal: string
  @ApiProperty()
  idadeAnimal: string
  @ApiProperty()
  tipoAnimal: string
  @ApiProperty()
  descricao: string
  @ApiProperty()
  imagemChamado: string
  @ApiProperty()
  latitudeChamado: number
  @ApiProperty()
  longitudeChamado: number
  @ApiProperty()
  enderecoChamado: string
  @ApiProperty()
  tipoChamado: string
  @ApiProperty()
  status: string
  @ApiProperty()
  createdAt: string
  @ApiProperty()
  updatedAt: string
}
