/**
 * Controller para a rota de chamados
 */
/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ChamadosService } from './chamados.service';
import { CreateChamadoDto } from './dto/create-chamado.dto';
import { UpdateChamadoDto } from './dto/update-chamado.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('chamados')
@ApiTags('Chamados')
@ApiResponse({ status: 200, description: 'OK' })
@ApiResponse({ status: 400, description: 'Bad Request' })
@ApiResponse({ status: 404, description: 'Not Found' })
@ApiResponse({ status: 500, description: 'Internal Server Error' })
export class ChamadosController {
  /**
   * Construtor da classe
   * @param chamadosService Service de chamados
   */
  constructor(private readonly chamadosService: ChamadosService) {}

  /**
   * Rota para criar um novo chamado
   * @param createChamadoDto Dados do chamado a ser criado
   * @returns O chamado criado
   */
  @Post('criar')
  @ApiResponse({ status: 201, description: 'Chamado criado com sucesso' })
  @ApiOperation({ summary: 'Criar um novo chamado' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiOperation({ summary: 'Rota Responsavel Por Criar Um Novos Chamado' })
  create(@Body() createChamadoDto: CreateChamadoDto) {
    return this.chamadosService.create(createChamadoDto);
  }

  /**
   * Rota para listar todos os chamados
   * @returns Uma lista de todos os chamados
   */
  @Get('listar-todos')
  @ApiResponse({ status: 200, description: 'Lista de chamados' })
  @ApiOperation({ summary: 'Listar todos os chamados' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiOperation({ summary: 'Rota Responsavel Por Listar Todos Os Chamado' })
  findAll() {
    return this.chamadosService.findAll();
  }

  /**
   * Rota para listar um chamado unico
   * @param id Id do chamado a ser listado
   * @returns O chamado listado
   */
  @Get(':id')
  @ApiResponse({ status: 200, description: 'Chamado listado com sucesso' })
  @ApiOperation({ summary: 'Listar um chamado unico' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiOperation({ summary: 'Rota Responsavel Por Listar Um  Unico Chamado' })
  findOne(@Param('id') id: string) {
    return this.chamadosService.findOne(+id);
  }

  /**
   * Rota para atualizar um chamado
   * @param id Id do chamado a ser atualizado
   * @param updateChamadoDto Dados do chamado a ser atualizado
   * @returns O chamado atualizado
   */
  @Patch('atualizar/:id')
  @ApiResponse({ status: 200, description: 'Chamado atualizado com sucesso' })
  @ApiOperation({ summary: 'Atualizar um chamado' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiOperation({ summary: 'Rota Responsavel Por Atualizar Um Chamado' })
  update(@Param('id') id: string, @Body() updateChamadoDto: UpdateChamadoDto) {
    return this.chamadosService.update(+id, updateChamadoDto);
  }

  /**
   * Rota para deletar um chamado
   * @param id Id do chamado a ser deletado
   * @returns Um objeto com a informa o de sucesso ou erro
   */
  @Delete('deletar/:id')
  @ApiResponse({ status: 200, description: 'Chamado deletado com sucesso' })
  @ApiOperation({ summary: 'Deletar um chamado' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiOperation({ summary: 'Rota Responsavel Por Deletar Um Chamado' })
  remove(@Param('id') id: string) {
    return this.chamadosService.remove(+id);
  }
}

/******  23b98825-2e6d-46a8-a1dc-5dd75dda0d83  *******/