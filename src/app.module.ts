/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ChamadosModule } from './chamados/chamados.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DenunciasModule } from './denuncias/denuncias.module';
import { FaleconoscoModule } from './faleconosco/faleconosco.module';

@Module({
  imports: [
    ChamadosModule,
    AuthModule,
    UsersModule,
    DenunciasModule,
    FaleconoscoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
